Source: node-ws
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Ximin Luo <infinity0@debian.org>, Jérémy Lal <kapouer@melix.org>
Testsuite: autopkgtest-pkg-nodejs
Build-Depends:
 debhelper-compat (= 13)
 , help2man (>= 1.47.1)
 , libnode-dev
 , mocha <!nocheck>
 , node-agent-base <!nocheck>
 , node-commander <!nocheck>
 , node-debug (>= 4.1.1+~cs4.1.5) <!nocheck>
 , node-expect.js <!nocheck>
 , node-gyp (>= 3.8.0-2)
 , node-nan (>= 2.0.5)
 , node-read <!nocheck>
 , node-should <!nocheck>
 , node-tinycolor
 , node-typescript
 , node-types-node
 , nodejs (>= 4.0)
 , dh-sequence-nodejs
Standards-Version: 4.5.1
Homepage: https://github.com/websockets/ws
Vcs-Browser: https://salsa.debian.org/js-team/node-ws
Vcs-Git: https://salsa.debian.org/js-team/node-ws.git
Rules-Requires-Root: no

Package: node-ws
Architecture: any
Depends:
 ${misc:Depends}
 , ${shlibs:Depends}
 , node-agent-base
 , node-commander
 , node-debug (>= 4.1.1+~cs4.1.5)
 , node-read
 , node-tinycolor
 , nodejs
Provides:
 ${nodejs:Provides}
Description: RFC-6455 WebSocket implementation module for Node.js
 ws is a simple to use websocket implementation, up-to-date against RFC-6455,
 and probably the fastest WebSocket library for Node.js.
 .
 Passes the quite extensive Autobahn test suite. See
 http://einaros.github.com/ws for the full reports.
 .
 It also provides wscat, a command-line tool which can either act
 as a server or a client, and is useful for debugging websocket services.
 .
 Node.js is an event-based server-side javascript engine.
